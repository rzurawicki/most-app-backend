from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import memcache

import feedparser
import json
import urllib
import logging

NUM_ITEMS = 10

feeds = {'news': 'http://news.google.com/news?ned=us&topic=h&output=rss',
         'videos': 'https://gdata.youtube.com/feeds/api/standardfeeds/most_popular?time=today'}

apis = {'images': ['http://api.pixable.com/api/streams/73/items?offset=0&limit=10&device=mobileWeb&nosession=1',
                   'http://news.yahoo.com/_xhr/photos/gallery-lightbox/?slideshowalias=photos-of-the-day-1340925511-slideshow&start=0&count=10&view=lightbox&_enable_feature_multimediagallery=1']}

# caches the feeds
def cache_feeds():
    parsedFeeds = {}
    parsedAPI = {}

    try:
        for feed in feeds:
            parsedFeeds[feed] = feedparser.parse(feeds[feed])
        for api in apis:
            parsedAPI[api] = []
            for url in apis[api]:
                data = json.loads(urllib.urlopen(url).read())
                parsedAPI[api].append(data)
    except BaseException as e:
        logging.error(e)
        return False


    # construct news
    newses = []
    for i in range(NUM_ITEMS):
        news = {'title': parsedFeeds['news'].entries[i].title.split(" - ")[1],
                'source': parsedFeeds['news'].entries[i].title.split(" - ")[-1],
                'snippet': parsedFeeds['news'].entries[i].summary.split('<font size="-1">')[2].split(
                    "<b>...</b></font><br />")[0],
                'link': parsedFeeds['news'].entries[i].link,
                'date': parsedFeeds['news'].entries[i].published,
                'thumbnail': parsedFeeds['news'].entries[i].summary.split('src="')[1].split('"')[0],
                'resultNumber': i + 1}

        newses.append(news)
    memcache.set('news', newses)

    # construct videos
    videos = []
    for i in range(NUM_ITEMS):
        video = {'title': parsedFeeds['videos'].entries[i].title,
                 'descr': parsedFeeds['videos'].entries[i].media_group,
                 'link': parsedFeeds['videos'].entries[i].link,
                 'date': parsedFeeds['videos'].entries[i].published,
                 'isMusic': 1,
                 'thumbnail': parsedFeeds['videos'].entries[i].media_thumbnail[0]['url'],
                 'resultNumber': i + 1}

        videos.append(video)
    memcache.set('videos', videos)

    # construct images
    images = []

    # do ten times
    for i in range(NUM_ITEMS):
        if i % 2 is 0:
            # pixable
            image = {'title': parsedAPI['images'][0]['data']['Stream']['Slide'][i / 2]['Caption'],
                     'link': parsedAPI['images'][0]['data']['Stream']['Slide'][i / 2]['ImageURL_Normal'],
                     'fullLink': parsedAPI['images'][0]['data']['Stream']['Slide'][i / 2]['ImageURL_Large'],
                     'date': parsedAPI['images'][0]['data']['Stream']['Slide'][i / 2]['Date'],
                     'resultNumber': i + 1}
        else:
            # yahoo
            image = {'title': parsedAPI['images'][1]['photos'][i / 2]['photo_title'],
                     'link': parsedAPI['images'][1]['photos'][i / 2]['url'],
                     'fullLink': parsedAPI['images'][1]['photos'][i / 2]['plink_vita'],
                     'date': parsedAPI['images'][1]['photos'][i / 2]['date'],
                     'resultNumber': i + 1}

        images.append(image)

    memcache.set('images', images)


def get_feed(feed):
    # gets the caches response, otherwise returns None
    return memcache.get(feed)


class MainPage(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write(
            "Welcome to the MOSTapp api, the following URLs are valid:\n\n/news\n/images\n/video\n/cache")


class CachePage(webapp.RequestHandler):
    def get(self):
        logging.info("Caching API request")
        if cache_feeds() is False:
            self.error(500)
            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write('There was an error accessing the remote API feeds. Caching failed!')
        else:
            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write("Feed cached successfully")


class FeedPage(webapp.RequestHandler):
    def get(self):
        # get that feed, remove the slash from the path
        data = get_feed(self.request.path[1:])

        # if there is no cache
        if data is None:
            self.error(503)
            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write(self.request.path)
            self.response.out.write("\nYour request could not be found in the cache, please try again later. ")

            # otherwise, output the data
        output = json.dumps(data)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(output)

application = webapp.WSGIApplication([('/', MainPage),
                                      ('/cache', CachePage),
                                      ('/news', FeedPage),
                                      ('/images', FeedPage),
                                      ('/videos', FeedPage)
], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()